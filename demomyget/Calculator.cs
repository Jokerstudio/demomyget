﻿using System;
using System.Collections.Generic;
using System.Text;

namespace demomyget
{
    public class Calculator
    {
        public double Sum(double operant1, double operant2)
        {
            return operant1 + operant2;
        }

        public double Multiply(double operant1, double operant2)
        {
            return operant1 * operant2;
        }

        public double Divide(double operant1, double operant2)
        {
            return operant1 / operant2;
        }
    }
}
